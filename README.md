# rust-pentagram

Pentagram is a graph library with a focus on ergonomics and minimalism.

## Goals

1. Create an ergonomic yet minimal library.
2. Encode the semantics of each type of graph (simple vs multi, undirected vs directed, etc) into a trait.
3. Provide a reference implementation for each trait.
4. Write each graph algorithm on top of a trait, so it works with multiple graph implementations.

## Design

Pentagram is the black sheep of Rust graph libraries. It is inspired by the dark arts of functional programming.

### Key Ideas

- Graphs are functors: nodes and edges contain associated data.
- Traversals and other algorithms are higher-order functions that allow flexible processing of associated data.
