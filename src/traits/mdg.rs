pub trait MultiDiGraph<'a, NI: 'a, ND: 'a, EI: 'a, ED: 'a> {
    type NodeIter: ExactSizeIterator<Item = (&'a NI, &'a ND)>;
    type EdgeIter: ExactSizeIterator<Item = (&'a NI, &'a NI, &'a EI, &'a ED)>;
    type NodeEdgeIter: ExactSizeIterator<Item = (&'a NI, &'a NI, &'a EI, &'a ED)>;

    fn insert_node(&mut self, id: NI, data: ND) -> Result<(), ()>;
    fn remove_node(&mut self, id: &NI) -> Option<ND>;
    fn insert_edge(&mut self, from: &NI, to: &NI, id: EI, data: ED) -> Result<(), ()>;
    fn remove_edge(&mut self, id: &EI) -> Option<(NI, NI, ED)>;
    fn nodes(&'a self) -> Self::NodeIter;
    fn edges(&'a self) -> Self::EdgeIter;
    fn node_data(&self, id: &NI) -> Option<&ND>;
    fn node_data_mut(&mut self, id: &NI) -> Option<&mut ND>;
    fn node_in_edges(&'a self, id: &NI) -> Option<Self::NodeEdgeIter>;
    fn node_out_edges(&'a self, id: &NI) -> Option<Self::NodeEdgeIter>;
    fn edge_data(&self, id: &EI) -> Option<&ED>;
    fn edge_data_mut(&mut self, id: &EI) -> Option<&mut ED>;
}
